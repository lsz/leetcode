class Solution {
public:
bool isValidBST(TreeNode* Root,int *max = nullptr,int *min = nullptr) {
   if(Root){ 
        int temp = Root->val;
        if(max&&temp>=*max || min&&temp<=*min)
            return false;

        return isValidBST(Root->left,&Root->val,min)&&\
            isValidBST(Root->right,max,&Root->val);
        
    }
    return true;
   }
};
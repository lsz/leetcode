/*
class Solution {
public:
    int climbStairs(int n) {
        if (n <= 2) 
            return n;
        vector<int> fb(n);
        fb[0] = 1; fb[1] = 2;
        for (int i = 2; i < n; ++i) {
            fb[i] = fb[i - 1] + fb[i - 2];
        }
        return fb.back();
    }
};
*/
class Solution{
public:
    int climbStairs(int n){
        
        if(n<=2) return n;
       
        int n1=1,n2=2,temp;
        n = n-2;
        while(n--){
            temp = n1+n2;
            n1 = n2;
            n2 = temp;   
        }             
        return temp; 
    }
};
class Solution {
public:
    int fib(int N) {
 /*      
        int pre = 0, cur = 1;
        for (int i = 2; i <= N; i ++) 
            cur += pre, pre = cur - pre;
        return (N == 0) ? 0 : cur;
 */       
       
        if (N <= 1)
        {
            return N;
        }
        if (N==2)
            return 1;
        
        int p1 = 1;
        int p2 = 1;
        int cur = 0;

        for (int i = 3; i <= N; ++i)
        {
            cur = p2 + p1;
            p2 = p1;
            p1 = cur;
        }

        return cur;
    
    }
};
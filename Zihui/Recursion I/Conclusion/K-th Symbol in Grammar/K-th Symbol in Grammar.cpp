class Solution {
public:
    int kthGrammar(int N, int K) {
        
       if(N==1 && K==1) return 0;
       int mid = pow(2,N-1)/2;  //lengh=pow(2,N-1)
       if(K<=mid){
           return kthGrammar(N-1,K);  //tail recursion
       } 
       else
           return !kthGrammar(N-1,K-mid);  //The left and right sides of mid are complementary codes.
    }
};


/*
class Solution {
public:
    int kthGrammar(int N, int K) {
        if (N == 1) {
            return 0;
        }
        int last_K = (K - 1) / 2;                   
        int remain = (K - 1) %2;                  
        int ans = kthGrammar(N - 1, last_K + 1);   
        if (ans == 0) {
            return remain == 0 ? 0 : 1;
        }
        else {
            return remain ==0 ? 1 : 0;
        }
    }
};
*/
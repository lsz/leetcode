//Divide and Conquer

class Solution{
public:
    vector<TreeNode*>generateTrees(int n){
        if(n==0) return {};
        vector<vector<TreeNode*>>dp(n+1);
        dp[0] = vector<TreeNode*>{ nullptr };
       // dp[1] = vector<TreeNode*>{ new TreeNode(1)};
        for(int i=1;i<=n;i++){
            for(int j=1;j<=i;j++){
                for(TreeNode* leftTree : dp[j-1]){
                    for(TreeNode* rightTree : dp[i-j]){
                        TreeNode* node = new TreeNode(j);
                        node->left = leftTree;
                        node->right = clone (rightTree, j);
                        dp[i].push_back(node);
                    }
                }                                
            }
        }
        return dp.back();
    }

private:
    TreeNode* clone(TreeNode* root, int offset){
        if (!root) return nullptr;
        TreeNode* temp = new TreeNode(root->val+offset);
        temp->left = clone(root->left,offset);
        temp->right = clone(root->right,offset);
        return temp;
    }
};

/*****recursive method
class Solution {
public:
	vector<TreeNode*> generateTrees(int n) {
		if (!n) return{};
		return generate(1, n);
	}
private:
	vector<TreeNode*> generate(int start, int end) {
		vector<TreeNode*> res;
		if (start > end) res.push_back(nullptr);
		else if (start == end) res.push_back(new TreeNode(start));
		else {
			for (int i = start; i <= end; ++i) {
				vector<TreeNode*> l = generate(start, i - 1);
				vector<TreeNode*> r = generate(i + 1, end);
				for(int j=0;j<l.size();++j)
					for (int k = 0; k < r.size(); ++k) {
						TreeNode* h = new TreeNode(i);
						h->left = l[j];
						h->right = r[k];
						res.push_back(h);
					}
			}
		}
		return res;
	}
};
*/
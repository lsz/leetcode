/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        //迭代
        ListNode* newHead = new ListNode(-1);   //open a front node.
        newHead->next = head;
        ListNode *pre = newHead, *cur = head;
        
        while(cur!=NULL&&cur->next!=NULL){
            pre->next = cur->next;
            cur->next = pre->next->next;
            pre->next->next = cur;
            
            pre=cur;
            cur=cur->next;
        }
        head = newHead->next;
        delete newHead;
        return head;
    }
};

/*递归---难理解
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        if (!head || !head->next) 
            return head;
        ListNode *t = head->next;
        head->next = swapPairs(head->next->next);
        t->next = head;
        return t;
    }
};
*/

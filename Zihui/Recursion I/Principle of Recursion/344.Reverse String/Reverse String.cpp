class Solution {
public:
    void reverseString(vector<char>& s){
        if(s.size()==0)
            return;   
        int left=0, right=s.size()-1;     //two pointers
        while(left<right)
            swap(s[left++],s[right--]);
    }
        
      /*  
        reverse(s.begin(),s.end());     //reverse();
       */ 
        
    /*   
    {
        helper(s.begin(), --s.end());   //Recursion   heap-buffer-overflow
    }
    void helper(vector<char>::iterator begin,vector<char>::iterator end)
    {
        if(begin==end) return;
        swap(*begin,*end);
        helper(begin+1,end-1); 
    }
    */
};
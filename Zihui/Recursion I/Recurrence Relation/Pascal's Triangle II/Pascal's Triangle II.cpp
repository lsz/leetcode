class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> res(rowIndex+1);
        for(int i = 0; i <= rowIndex; i++){
            for(int j = i; j >= 0; j--){
                if(j == 0 || j == i) res[j] = 1;
                else{
                    res[j] = res[j] + res[j-1];
                }
            }
        }
        return res;
    }
};

/*
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> rec(1,1);
        while(rowIndex--)
        {
            rec.push_back(1);

            for(int i=rec.size()-2;i>=1;i--)
                rec[i]=rec[i]+rec[i-1];
        }
        return rec;
    }
};
*/
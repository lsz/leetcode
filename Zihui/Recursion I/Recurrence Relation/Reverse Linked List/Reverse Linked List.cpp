class Solution {
public:
    ListNode* reverseList(ListNode* head) {
	
		//迭代
        if(head==nullptr)
            return head;
        ListNode *tmp = head->next;
        ListNode *prenode = head;
        while(tmp!=NULL)
        {  
            ListNode *nextnode = tmp->next;
            tmp->next = prenode;
            prenode = tmp;
            tmp = nextnode;
         }
         head->next = NULL;
         head = prenode;
         return head;
    }
};
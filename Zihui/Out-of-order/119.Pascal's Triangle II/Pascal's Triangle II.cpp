class Solution {
public:
    vector<int> getRow(int rowIndex) {
        vector<int> rec(1,1);
        while(rowIndex--)
        {
            rec.push_back(1);

            for(int i=rec.size()-2;i>=1;i--)
                rec[i]=rec[i]+rec[i-1];
        }
        return rec;
    }
};
class Solution {
public:
    void reverseString(vector<char>& s) {
        
       if(s.size()==0) 
             return ;
         int left = 0, right = s.size() - 1;
         while (left < right) {
            swap(s[left++], s[right--]);
        } 
        
    /*    for(int i = 0, j = s.size()-1; i < s.size()/2; i++, j--)
        {
            swap(s[i],s[j]);
        }
    */
    }
};
class Solution {
public:
   vector<int> twoSum(vector<int> &nums, int target){
    vector<int> twoSum;
    map<int, int> tmpmap;
    for(int i=0; i<nums.size(); ++i) {
        if(tmpmap.count(nums[i]) != 0) {
          twoSum.push_back(tmpmap[nums[i]]);
          twoSum.push_back(i);
        break;
     }
     tmpmap[target - nums[i]] = i;
   }
   return twoSum;
  }
};
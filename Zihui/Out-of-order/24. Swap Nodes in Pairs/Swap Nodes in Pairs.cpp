/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
		ListNode *newHead = new ListNode(0);    //open up a front node
		newHead->next = head;
		ListNode *pre = newHead, *cur = head;
		int cnt = 1;
		while (cur != NULL && cur->next != NULL) {
			
			pre->next = cur->next;  // swap cur and cur->next
			cur->next = pre->next->next;
			pre->next->next = cur;
			
			pre = cur;  // go over two nodes
			cur = cur->next;
		}
		head = newHead->next;
		delete newHead;
		return head;
    }
};
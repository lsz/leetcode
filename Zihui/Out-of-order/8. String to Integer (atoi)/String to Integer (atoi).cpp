class Solution {
public:
    int myAtoi(string str) {
        int n = str.size();
        int i = 0;
        int sign = 1;
        while (i < n && str[i] == ' '){
            i++;
        }
        
        if (i == n) return false;
        if (str[i] == '+' || str[i] == '-'){
            sign = (str[i] == '+') ? 1 : -1;
            i++;
        }
  
        if (str[i] < '0' || str[i] > '9') return 0;
      
        int digit = 0;
        
        while (i < n && str[i] >= '0' && str[i] <= '9'){ 
            if (digit > INT_MAX / 10 || digit == INT_MAX / 10 && str[i] - '0' > 7){
                return sign > 0 ? INT_MAX : INT_MIN;
            }
            digit = digit * 10 + (str[i] - '0');
            i++;
        }
        return sign * digit;
    }
};
class Solution {
public:
    int fib(int N) {
        
        int pre = 0, cur = 1;
        for (int i = 2; i <= N; i ++) 
            cur += pre, pre = cur - pre;
        return (N == 0) ? 0 : cur;
       
        /*
        if (N <= 1)
        {
            return N;
        }

        int p0 = 0;
        int p1 = 1;
        int p2 = 1;

        for (int i = 2; i < N; ++i)
        {
            int tmp = p2 + p1;
            p0 = p1;
            p1 = p2;
            p2 = tmp;
        }

        return p2;
        */ 
    }
};
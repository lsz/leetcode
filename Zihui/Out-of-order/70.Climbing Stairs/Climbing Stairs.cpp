class Solution {
public:
    int climbStairs(int n) {
        if (n <= 1) 
            return 1;
        vector<int> fb(n);
        fb[0] = 1; fb[1] = 2;
        for (int i = 2; i < n; ++i) {
            fb[i] = fb[i - 1] + fb[i - 2];
        }
        return fb.back();
    }
};
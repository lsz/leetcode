class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        
        //从尾端比较
		int i = m - 1, j = n - 1, len = m + n - 1;
		while (i >= 0 && j >= 0)
		{
			if (nums1[i] <= nums2[j])
			{
				nums1[len--] = nums2[j--];

			}
			else
			{
				nums1[len--] = nums1[i--];

			}

		}
		if (i < 0)
		{
			for (int p = 0; p <= j; p++)
				nums1[p] = nums2[p];
		}


    }
};

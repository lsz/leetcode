class Solution {
public:
    int kthGrammar(int N, int K) {
        if (N == 1) {
            return 0;
        }
        int last_K = (K - 1) / 2;                   
        int remain = (K - 1) % 2;                  
        int ans = kthGrammar(N - 1, last_K + 1);   
        if (ans == 0) {
            return remain == 0 ? 0 : 1;
        }
        else {
            return remain == 0 ? 1 : 0;
        }
    }
};
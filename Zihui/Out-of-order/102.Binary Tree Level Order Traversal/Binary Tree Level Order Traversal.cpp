class Solution {
public:
    vector <vector<int>> levelOrder(TreeNode *root) {
        vector <vector<int>> result;
        if (root == NULL) { return result ; }
        vector <TreeNode*> nodeStack;
 
        nodeStack.push_back(root);
        while (nodeStack.size()) {
            vector<int> value;
            vector < TreeNode *> nextLevel;
            for (int i = 0; i < nodeStack.size(); i++) {
                value.push_back(nodeStack[i]->val);
            }
            for (int i = 0; i < nodeStack.size(); i++) {
                if (nodeStack[i]->left) { nextLevel.push_back(nodeStack[i]->left); }
                if (nodeStack[i]->right) { nextLevel.push_back(nodeStack[i]->right); }
            }
            nodeStack = nextLevel;
            result.push_back(value);
        }
        return result;
 
 
    }
};
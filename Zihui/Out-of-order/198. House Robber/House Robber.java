public class Solution {
    public int rob(int[] nums) {
        if(nums == null || nums.length == 0){
            return 0;
        }
        int len = nums.length;
        if(len == 1){
            return nums[0];
        }
        int[] M = new int[len];
        M[0] = nums[0];
        M[1] = Math.max(nums[0], nums[1]);
        for(int i = 2; i < len; i++){
            M[i] = Math.max(M[i-1],M[i-2]+nums[i]);
        }
        return M[len-1];
    }
}
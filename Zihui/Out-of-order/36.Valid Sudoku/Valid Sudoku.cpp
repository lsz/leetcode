class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        int row[9][10] = {0}, col[9][10] = {0}, subBox[9][10] = {0};

        for(int i = 0;i<9;i++){
            for(int j = 0;j<9;j++){
                int n = board[i][j] == '.'? 0 : board[i][j]-'0';
                if(n == 0)
                    continue;
                if(++row[i][n] > 1)
                    return false;
                if(++col[j][n] > 1)
                    return false;
                int subIndex = i/3*3 + j/3;
                if(++subBox[subIndex][n] > 1)
                    return false;
            }
        }
        return true;
    }
};
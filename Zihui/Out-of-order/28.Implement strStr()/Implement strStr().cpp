class Solution {
public:
    int strStr(string haystack, string needle) {
        int i = haystack.size();
        int j = needle.size();
        if(!j)
            return 0;

        for(int a=0;a<i;a++)
        {
            int flag = 1;
            string temp = haystack.substr(a,j);
            if(temp==needle)
                return a;
        }
        return -1;
    }
};
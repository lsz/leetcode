class Solution {
    public TreeNode sortedArrayToBST(int[] nums) {
        if(nums.length==0){
        return null;
    }
    
    int m = nums.length/2;
    TreeNode tree = new TreeNode();
    tree.val = nums[m];
    if(m>0){
        tree.left = sortedArrayToBST(Arrays.copyOfRange(nums, 0, m));
        tree.right = sortedArrayToBST(Arrays.copyOfRange(nums, m+1, nums.length));
    }
    return tree;
    }
}
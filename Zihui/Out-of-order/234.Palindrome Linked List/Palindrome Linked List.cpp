class Solution {
public:
    bool isPalindrome(ListNode* head) {
        if(head == NULL)return true;
        
        vector<int>v;
        while(head){
            v.push_back(head->val);
            head = head->next;
        }
        int len = v.size();
        int a = len/2;
        for(int i=0; i <a; i++){
            if(v[i] != v[len-i-1])
                return false;
        }
        return true;
    }
};
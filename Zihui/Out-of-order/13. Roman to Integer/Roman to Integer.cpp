class Solution {
public:
    int romanToInt(string s) 
    {
        map<char,int>q;
        q['I']=1;
        q['X']=10;
        q['C']=100;
        q['M']=1000;
        q['V']=5;
        q['L']=50;
        q['D']=500;
        int res = 0;
        res = q[s[0]];
        for(int i=1;i<s.size();i++)
        {
            if(q[s[i-1]]>=q[s[i]])
                res+=q[s[i]];
            else                  
                res = res + q[s[i]] - 2*q[s[i-1]];    
        }
        return res; 
    }
};
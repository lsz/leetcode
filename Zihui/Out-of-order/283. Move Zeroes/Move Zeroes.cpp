class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        
        int pre=0,cur=0;//pre指向数为0，cur指向非0
        int n = nums.size();
        for(int cur = 0;cur < n;cur++){
           if(nums[cur]){
               nums[pre++]  = nums[cur];
           }
        }
        
         for(int i = pre;i < n; i++){
            nums[i] = 0;  
         }
    }
    
};
public class Solution {
    public boolean isAnagram(String s, String t) {
        char[] i = s.toCharArray();
        char[] j = t.toCharArray();
        
        Arrays.sort(i);
        Arrays.sort(j);
        
        return String.valueOf(i).equals(String.valueOf(j));
    }
}
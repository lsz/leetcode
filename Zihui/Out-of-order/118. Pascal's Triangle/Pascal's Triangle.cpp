class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> triangle;
        
        for (int i = 1; i <= numRows; ++i){
            vector<int> t(i,1);
            triangle.push_back(t);
        }
        
        for (int i = 3; i <= numRows; ++i){
            for (int j = 1; j < i-1; ++j){
                triangle[i-1][j] = triangle[i-2][j-1] + triangle[i-2][j];
            }
        }
        
        return triangle;
    }
};
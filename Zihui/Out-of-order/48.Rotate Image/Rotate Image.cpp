class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
     /* if (matrix.empty())
            return;

        int n = matrix.size();
        for (int i = 0; i < n; i++)//沿主对角线交换元素
        {
            for (int j = 0; j <= i; j++)
                swap(matrix[i][j], matrix[j][i]);
        }

        for (int i = 0, j = n - 1; i < j; i++, j--)//按对称列进行交换
        {
            for (int k = 0; k < n; k++)
                swap(matrix[k][i], matrix[k][j]);
        }
     */
        //上下翻转，沿主对角线交换
        reverse(matrix.begin(), matrix.end());
        for (int i = 0; i < matrix.size(); ++i) 
        {
            for (int j = i + 1; j < matrix[i].size(); ++j)
                swap(matrix[i][j], matrix[j][i]);
        }

    }
};
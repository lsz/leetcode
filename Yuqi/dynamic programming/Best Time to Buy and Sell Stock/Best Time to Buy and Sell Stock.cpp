class Solution {
public:
    int maxProfit(vector<int>& prices) {
        if(prices.empty()) return 0;
        
        int min_prices=prices[0];
        int nums=0;
        
        for(int i=0;i<prices.size();i++){
            min_prices=min(min_prices,prices[i]);
           nums=max(nums,prices[i]-min_prices);
        } 
        return nums;
    }
};
class Solution {
public:
    int rob(vector<int>& nums) {
        if(nums.size()==0) return 0;
        if(nums.size()==1) return nums[0];
        int premax=nums[0];
        int curmax=max(nums[0],nums[1]);
        int res=curmax;
        for(int i=2;i<nums.size();i++){
            int t=curmax;
            curmax=max(nums[i]+premax,curmax);
            premax=t;
            res=max(res,curmax);
        }
        return res;
    }
};
class Solution {
    vector<int> c;
    vector<int> nums;
public:
    Solution(vector<int>& nums) {
       this->nums.assign(nums.begin(),nums.end());
        this->c=nums;
    }
    
    /** Resets the array to its original configuration and return it. */
    vector<int> reset() {
       nums.clear();
        nums.assign(c.begin(),c.end());
        return nums;
    }
    
    /** Returns a random shuffling of the array. */
    vector<int> shuffle() {
        int len=nums.size();
        for(int i=len-1;i>=0;--i){
            int r=rand()%(i+1);
            swap(nums[r],nums[i]);
        }
        return nums;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(nums);
 * vector<int> param_1 = obj->reset();
 * vector<int> param_2 = obj->shuffle();
 */
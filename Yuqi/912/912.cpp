class Solution {
public:
    vector<int> sortArray(vector<int>& nums) {
        quickSort(nums,0,nums.size()-1);
        return nums;
    }
    
    void quickSort(vector<int>& nums, int start,int end){
        if(start >= end)
            return;
        int mid=part(nums,start,end);
        quickSort(nums,start,mid-1);
        quickSort(nums,mid+1,end);
    }
    
    int part(vector<int>& nums,int start,int end){
        int pi=nums[start];
        int left=start+1;
        int right=end;
       while(left<=right){
            while(left<=right && nums[left]<=pi) left++;
            while(left<=right && nums[right]>=pi) right--;
            if(left>right) break;
                swap(nums[left],nums[right]);
            } 
        swap(nums[right],nums[start]);
    return right;
    }
    
};
class Solution {
public:
    vector<string> generateParenthesis(int n) {
       vector<string> res;
        int l=0,r=0;
        dfs(res,"", n ,l ,r);
        return res;
    }

    void dfs(vector<string>& res, string path,int n, int l,int r){
        if(r>l || l >n || r>n) return;
        if(l ==r && l==n){
            res.push_back(path);
            return;
        }
        
        dfs(res, path+'(',n,l+1, r);
        dfs(res, path +')', n, l,r+1);
    }
};
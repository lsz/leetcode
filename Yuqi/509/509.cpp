class Solution {
public:
    int fib(int N) {
        if(N<1) return 0;
        vector<int > me(N+1,0);
        return help(me,N);
    }
    
    int help(vector<int>& me,int n){
        
        if(n==1 || n==2) return 1;
        if(me[n]!= 0)
            return me[n];
        me[n]=help(me,n-1)+help(me,n-2);
        return me[n];
    }
};
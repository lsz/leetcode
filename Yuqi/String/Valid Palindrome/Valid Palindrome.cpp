class Solution {
public:
    bool isPalindrome(string s) {
        string sg;
        for(char ch:s){
            if(isalnum(ch))
                sg+=tolower(ch);
        }
        
        int n=sg.size();
        int left=0,right=n-1;
        
        while(left<right){
            if(sg[left] != sg[right])
                return false;
            
            ++left;
            --right;
        }
        return true;
    }
};
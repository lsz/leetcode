class Solution {
public:
        vector<vector<int>> res;    
    
    vector<vector<int>> combine(int n, int k) {
        if(k<=0 || n<=0) return res;
        vector<int> track;
        back(n,k,1,track);
        return res;
    }
    
    void back(int n,int k,int start, vector<int>& track){
        if(k==track.size()){
            res.push_back(track);
            return;
        }
        
        for(int i=start;i<=n;i++){
            track.push_back(i);
            back(n,k,i+1,track);
            track.pop_back();
        }
    }
    
};
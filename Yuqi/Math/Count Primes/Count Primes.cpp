class Solution {
public:
    int countPrimes(int n) { 
        if(n<3) return false;
        vector<bool> num(n,true);
        int count=0;
        
        num[0]=false;
        num[1]=false;
        
        for(int i=2;i<sqrt(n);i++)
            if(num[i])
        for(int j=i*2;j<n;j+=i)
        num[j]=false;
        
        
    for(int i=2; i<n ;i++)
    {if(num[i]) ++count;}
    return count;
    }
};
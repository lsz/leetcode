class Solution {
public:
    vector<vector<int>> res;
     void backtrack(vector<int>& nums, vector<int>& track, vector<bool>& nums1){
        if(track.size()== nums1.size()){
            res.push_back(track);
                     }
       else {
        for(int i=0; i<nums.size();i++){
            if(nums1[i]){
           track.push_back(nums[i]);
                nums1[i]=false;
            backtrack(nums,track,nums1);
                nums1[i]=true;
            track.pop_back();
        }
        }
    }
     }
    vector<vector<int>> permute(vector<int>& nums) {
        if(nums.size()==0) return {};
        else if(nums.size()==1)
            return {{nums[0]}};
        else{
            vector<bool> nums1(nums.size(),true);
        vector<int> track;
        backtrack(nums,track,nums1);
        return res;
    }
    
    }
};
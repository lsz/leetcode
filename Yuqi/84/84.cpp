class Solution {
public:
    int largestRectangleArea(vector<int>& heights) {
        int res=0;
        for(int i=0;i<heights.size();i++){
           if(i+1<heights.size() && heights[i]<=heights[i+1] ){
           continue;    
        }
        
        int minh=heights[i];
            for(int j=i;j>=0;j--){
               minh=min(minh,heights[j]);
                int are=minh*(i-j+1);
                res=max(res,are);
                
            }
    }
        return res;
    }
};
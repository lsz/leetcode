class MinStack {
   stack<int> dataStack;
    stack<int> minStack;
    
public:
    /** initialize your data structure here. */
    MinStack() {
        minStack.push(INT_MAX);
    }
    
    void push(int x) {
        dataStack.push(x);
            if(minStack.empty() || x<=minStack.top())
                minStack.push(x);
        else minStack.push(min(minStack.top() , x));
    }
    
    void pop() {
        dataStack.pop();
        
            minStack.pop();
    }
    
    int top() {
        return dataStack.top();
    }
    
    int getMin() {
        return minStack.top();
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        uint32_t temp = 0;
        for(int i = 0;i < 32;i++){
            temp = temp*2 + n%2;
            n = n/2;
        }
        return temp;

    }
};
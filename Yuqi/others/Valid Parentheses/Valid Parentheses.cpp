class Solution {
public:
    bool isValid(string s) {
        stack<int> t;
        bool flag=true;
        
        for(int i=0;i<s.length();i++){
            if(s[i]=='[' || s[i]=='(' || s[i]=='{' )
                t.push(s[i]);
            else{
                if(t.empty())
                {
                    flag=false;
                 break;   
                }
            char c;
            if(s[i]==']')
                c='[';
            else if(s[i]==')')
                c='(';
            else c='{';
            
            if(t.top()!= c){
                flag = false;
                break;
            }
            
            t.pop();
        }
        }
        if(t.empty())
            return flag;
        else 
            return false;
        
    }
};
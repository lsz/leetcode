class Solution {
public:
    vector<vector<int>> generate(int numRows) {
       vector<vector<int>> p;
        if(numRows==0)
            return p;
        
        vector<int> f(1,1);
        p.push_back(f);
        if(numRows==1)
            return p;
        
        for(int i=2;i<=numRows;i++){
        f.push_back(0);
            vector<int> q=f;
            
            for(int j=0;j<i;j++){
                if(j==0) q[j]=f[j];
                else q[j]=f[j-1]+f[j];
            }
            p.push_back(q);
            f=q;
            
        }
        return p;
        
    }
};
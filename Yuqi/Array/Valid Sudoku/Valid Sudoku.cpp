class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        int r[9][10]= {0};
        int c[9][10]= {0};
        int b[9][10]= {0};
        for(int i=0; i<9 ;i++){
            for(int j=0; j<9; j++){
                if(board[i][j]== '.') continue;
                int cur = board[i][j]-'0';
                if(r[i][cur]) return false;
                if(c[j][cur]) return false;
                if(b[j/3 + (i/3)*3][cur])
                    return false;
                
                r[i][cur]=1;
                c[j][cur]=1;
                b[j/3 + (i/3)*3][cur]=1;
            }
        }
        
        return true;
    }
};
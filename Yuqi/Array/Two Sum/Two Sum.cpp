class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        vector<int> a;
        vector<int> t;
        t=nums;
        int n=nums.size();
        sort(t.begin(),t.end());
        int i=0,j=n-1;
        
        while(i<j){
            if(t[i]+t[j]>target) j--;
            else if(t[i]+t[j]<target) i++;
            else break;
        }
        
        if(i<j){
            for(int k=0;k<n;k++){
                if(i<n && nums[k]==t[i]){
                    a.push_back(k);
                    i=n;
                }
                
                else if(j<n && nums[k]==t[j]){
                    a.push_back(k);
                    j=n;
                }
            if(i==n && j==n)
            return a;
            }
        }
        return a;
    }
};
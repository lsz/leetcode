class Solution {
public:
    double myPow(double x, int n) {
        if(n ==0 || x==1) return 1;
        if(n==1)  return x;
        if(n<0) return 1/(x*myPow(x,-(n+1)));
        double r=1;
        while(n>1){
            if(n%2 ==1)
                r*=x;
            x=x*x;
            n/=2;
        }
        r*=x;
        return r;
    }
};